extern crate embedded_hal;
extern crate linux_embedded_hal as hal;
#[macro_use(block)]
extern crate nb;
extern crate ads1x1x;

use embedded_hal::adc::OneShot;
use ads1x1x::{ Ads1x1x, SlaveAddr, channel };

/// LM35DZ is analogue temperature sensor
/// Temperature range is from -55 to 150 Celcius degree
/// Precision is 0.5 Celcius degree
/// Scale is linear 10 mV / Celcius degree
/// Supply voltage is from 4 V to 30 V
//
fn lm35dz_scale(measurement: f32) -> f32 {

    // Convert input integer into floating point number
    // let measurement = measurement as f32;

    // Scale
    let temperature = (150.0 - -55.0) * measurement / 65535.0 / 2.0 - 55.0;

    // Return calculated value
    temperature
}

fn main() {

    let dev = hal::I2cdev::new("/dev/i2c-1").unwrap();
    let address = SlaveAddr::default();
    let mut adc = Ads1x1x::new_ads1115(dev, address);

    // All input sensors are supplied by 5 Volts
    // So set gain amplifier to have measurements in range of 6 Volts
    adc.set_full_scale_range(ads1x1x::FullScaleRange::Within6_144V).unwrap();

    let measurement = block!(adc.read(&mut channel::SingleA0)).unwrap();
    println!("Measurement: {}", measurement);

    // Normalize 6.144V adc input voltage to max 5V for sensor
    let measurement: f32 = measurement as f32 * 6.144 / 5.0;

    let temperature = lm35dz_scale(measurement);
    println!("Temperature: {:.1}", temperature);

    let _dev = adc.destroy_ads1115(); // get I2C device back

}
